const router = require('express').Router();
const User = require('./../models/USer');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const passport = require('passport');

require('./../passport-setup');


// Register
router.post('/register', (req, res, next) => {
	let {password, confirmPassword} = req.body;

	if(password !== confirmPassword){
		return res.status(400).send({
			error: "Password and confirm password do not match"
		})
	}

	if(password.length < 8){
		return res.status(400).send({
			error: "Password should be at least 8 characters"
		})
	}

	// this is will how many times to hash the password
	const saltRounds = 10;

	bcrypt.genSalt(saltRounds, function(err, salt) {
		bcrypt.hash(password, salt, function(err, hash) {
			req.body.password = hash;

			User.create(req.body)
				.then(user => res.send(user))
				.catch(next)
		})
	})
});


// Login

router.post('/login', (req, res, next) => {
	// check if field are not empty
	let {email, password} = req.body;

	if(!email || !password){
		return res.status(400).send({
			error: "Check your credentials"
		})
	}

	// check if the email is existing
	User.findOne({email})
		.then(user => {
			if(!user){
				res.status(400).send({
					error: "Please check your credentials"
				})
			}else {
			// compare password given by user and hashed password in db
			// Load hash from your password DB.
			bcrypt.compare(password, user.password)
				.then(result => {
					console.log(result);

					if(result){
			      // if comparing is successful
			     // create a token then send user details
			     		let {_id, fullname, email, isAdmin} = user
			     		let token = jwt.sign({_id: user._id}, 'secret_key')

			     		res.send({
			     			message: "Login successfully",
			     			token,
			     			user:{
			     				_id,
			     				fullname,
			     				email,
			     				isAdmin
			     			}
			     		})
					}else {
						res.status(400).send({
							error: "Please check your credentials"
						})
					}
				})
			}
		})
})


router.get('/profile',
 passport.authenticate('jwt', {session: false }), 
 (req, res, next) => {
	res.send(req.user);
})
module.exports = router